#include <Wire.h>
#include <Arduino.h>
#include <PWMServo.h>

PWMServo polar_servo;
PWMServo azimuth_servo;

/*
  The model uses a spherical coordinate system in degrees.
 */
const auto polar_pin = PPOLAR;
const auto azim_pin = PAZIM;
const auto range_pin = PRANGE;

void setup() {
  pinMode(range_pin, INPUT);
  pinMode(polar_pin, OUTPUT);
  pinMode(azim_pin, OUTPUT);
  polar_servo.attach(polar_pin);
  azimuth_servo.attach(azim_pin);
}

/**
 * Read write from serial loop.
 * The servo values are read and actuated, and the rangefinder analog reading is written.
 */
void loop()
{
  auto polar = 0;
  while (not polar)
    {
      const auto scanned = Serial.parseInt();
      polar = scanned ? scanned : polar;
    }

  auto azimuth = 0;
  while (not azimuth)
    {
      const auto scanned = Serial.parseInt();
      azimuth = scanned ? scanned : azimuth;
    }

  polar_servo.write(polar);
  azimuth_servo.write(azimuth);
  Serial.println(analogRead(range_pin));
}

